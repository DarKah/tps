# TP1 : Programmatic provisioning

# I. Une première VM

## 1. ez startup

➜ *Vous pouvez aussi constater que la VM est allumée dans votre hyperviseur.*

🌞 **`Vagrantfile` dans le dépôt git de rendu SVP !**

Je viens de le mettre sous le nom "Vagrantfile 1. ez startup"

## 2. Un peu de conf

Avec Vagrant, il est possible de gérer un certains nombres de paramètres de la VM.

🌞 **Ajustez le `Vagrantfile` pour que la VM créée** :

- ait l'IP `10.1.1.11/24`
- porte le hostname `ezconf.tp1.efrei`
- porte le nom (pour Vagrant) `ezconf.tp1.efrei` (ce n'est pas le hostname de la machine)
- ait 2G de RAM
- ait un disque dur de 20G

Sous le nom "Vagrantfile 2. Un peu de conf"

# II. Initialization script

🌞 **Ajustez le `Vagrantfile`** :

- quand la VM démarre, elle doit exécuter un script bash
- le script installe les paquets `vim` et `python3`
- il met aussi à jour le système avec un `dnf update -y` (si c'est trop long avec le réseau de l'école, zappez cette étape)
- ça se fait avec une ligne comme celle-ci :

```bash
#contenu de mon setup.sh
echo "Downloading Nano"
    sudo dnf install nano -y
    
echo "Downloading Py"
    sudo dnf install python3 -y

echo "Updating"
    sudo dnf update -y
```

+ Mon Vagrantfiel sous le nom de "Vagrantfile 3 Initialization script"

# III. Repackaging


🌞 **Repackager la VM créée précédemment**

```bash
$ vagrant package --output ezconf-tp1-efrei

$ vagrant box add ezconf-tp1-efrei ezconf-tp1-efrei.box
```



# IV. Multi VM


🌞 **Un deuxième `Vagrantfile` qui définit** :

le Vagrantfile s'appelle "Vagrantfile Multi"

🌞 **Une fois les VMs allumées, assurez-vous que vous pouvez ping `10.1.1.102` depuis `node1`**

```bash

[vagrant@node1 ~]$ ping 10.1.1.102
PING 10.1.1.102 (10.1.1.102) 56(84) bytes of data.
64 bytes from 10.1.1.102: icmp_seq=2 ttl=64 time=1.70 ms
64 bytes from 10.1.1.102: icmp_seq=3 ttl=64 time=1.83 ms

```

# V. cloud-init

🌞 **Repackager une box Vagrant**

```bash
#Ajouté à setup.sh
echo "Downloading Cloud Init"
    sudo dnf install cloud-init -y
    sudo systemctl enable cloud-init

#ensuite
vagrant package --output Cloudinit
vagrant box add Cloudinit.box Cloudinit
```



🌞 **Tester !**

"Vagranfile Cloudinit" et j'ai pris l'étape A car Problème sur Windows ^^'

```bash

PS D:\Cours\TPLeo> vagrant ssh
Last login: Thu Apr  4 20:16:44 2024 from 10.0.2.2
[vagrant@cloud ~]$ ls /home
lala  vagrant
```