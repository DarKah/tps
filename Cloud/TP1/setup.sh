echo "Updating"
    sudo dnf update -y

echo "Downloading Nano and Python3"
    sudo dnf install python3 -y
    sudo dnf install nano -y

echo "Downloading Cloud Init"
    sudo dnf install cloud-init -y
    sudo systemctl enable cloud-init