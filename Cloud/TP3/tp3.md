# II.1. Setup Frontend

## A. Database

🌞 **Installer un serveur MySQL**

- on va installer une version spécifique de MySQL, demandée par OpenNebula
- **ajouter un dépôt :**
  - télécharger le RPM disponible à l'URL suivante : https://dev.mysql.com/get/mysql80-community-release-el9-5.noarch.rpm 
  - puis installez-le localement avec une commande `rpm`
```bash
[vagrant@frontend ~]$ sudo !!
sudo rpm -i mysql80-community-release-el9-5.noarch.rpm 
warning: mysql80-community-release-el9-5.noarch.rpm: Header V4 RSA/SHA256 Signature, key ID 3a79bd29: NOKEY

```

- **installer le paquet qui contient le serveur MySQL depuis ce nouveau dépôt :**
  - faites un `dnf search mysql` pour voir les paquets dispos
  - vous devriez voir un paquet `mysql-community-server` dispo
  - installez-le

```bash
[vagrant@frontend ~]$ sudo !!
sudo dnf install mysql-community-server
```

🌞 **Démarrer le serveur MySQL**

- démarrer le service `mysqld`
- activer ce service au démarrage

```bash
[vagrant@frontend ~]$ sudo systemctl start mysqld
[vagrant@frontend ~]$ sudo systemctl enable mysqld
```

🌞 **Setup MySQL**

```SQL
ALTER USER 'root'@'localhost' IDENTIFIED BY '*********';
Query OK, 0 rows affected (0.02 sec)

CREATE USER 'oneadmin' IDENTIFIED BY '**********';
Query OK, 0 rows affected (0.02 sec)

CREATE DATABASE opennebula;
Query OK, 0 rows affected (0.00 sec)

GRANT ALL PRIVILEGES ON opennebula.* TO 'oneadmin';
Query OK, 0 rows affected (0.00 sec)

SET GLOBAL TRANSACTION ISOLATION LEVEL READ COMMITTED;
Query OK, 0 rows affected (0.00 sec)

```

## B. OpenNebula

🌞 **Ajouter les dépôts Open Nebula**

```bash
[vagrant@frontend ~]$ sudo nano /etc/yum.repos.d/rocky.repo
[vagrant@frontend ~]$ dnf makecache -y
```

🌞 **Installer OpenNebula**

```
[vagrant@frontend ~]$ sudo dnf install opennebula opennebula-sunstone opennebula-fireedge
```

🌞 **Configuration OpenNebula**

```conf
DB = [ BACKEND = "mysql",
       SERVER  = "localhost",
       PORT    = 0,
       USER    = "oneadmin",
       PASSWD  = "**********",
       DB_NAME = "opennebula",
       CONNECTIONS = 25,
       COMPARE_BINARY = "no" ]
```

🌞 **Créer un user pour se log sur la WebUI OpenNebula**

```bash
[vagrant@frontend ~]$ sudo su - oneadmin
Last login: Tue Apr  9 10:26:31 UTC 2024
[oneadmin@frontend ~]$ nano /var/lib/one/.one/one_auth
```

🌞 **Démarrer les services OpenNebula**

```bash
[vagrant@frontend ~]$ sudo systemctl start opennebula opennebula-sunstone
[vagrant@frontend ~]$ 
```

## C. Conf système

🌞 **Ouverture firewall**

- ouvrez les ports suivants, avec des commandes `firewall-cmd` :

````bash 
[vagrant@frontend ~]$ sudo firewall-cmd --permanent --zone=public --add-port=9869/tcp
success
[vagrant@frontend ~]$ sudo firewall-cmd --permanent --zone=public --add-port=22/tcp
success
[vagrant@frontend ~]$ sudo firewall-cmd --permanent --zone=public --add-port=2633/tcp
success
[vagrant@frontend ~]$ sudo firewall-cmd --permanent --zone=public --add-port=4124/tcp
success
[vagrant@frontend ~]$ sudo firewall-cmd --permanent --zone=public --add-port=4124/udp
success
[vagrant@frontend ~]$ sudo firewall-cmd --permanent --zone=public --add-port=29876/tcp
success
[vagrant@frontend ~]$ sudo firewall-cmd --reload
success
 ````

| Port  | Proto      | Why ?                        |
|-------|------------|------------------------------|
| 9869  | TCP        | WebUI (Sunstone)             |
| 22    | TCP        | SSH                          |
| 2633  | TCP        | Daemon `oned` et API XML RPC |
| 4124  | TPC et UDP | Monitoring                   |
| 29876 | TCP        | NoVNC proxy                  |

## D. Test

A ce stade, vous devriez déjà pouvoir **visiter la WebUI de OpenNebula.**

**RDV sur `http://10.3.1.11:9869`** avec un navigateur ! Vous pouvez vous log avec les identifiants définis dans la partie B.

# II. 2. Noeuds KVM

## A. KVM

🌞 **Ajouter des dépôts supplémentaires**

```bash
sudo rpm -i mysql80-community-release-el9-5.noarch.rpm 
warning: mysql80-community-release-el9-5.noarch.rpm: Header V4 RSA/SHA256 Signature, key ID 3a79bd29: NOKEY


dnf install -y epel-release
```

🌞 **Installer KVM**

- un paquet spécifique qui vient des dépôts OpenNebula : `opennebula-node-kvm`
````bash
[vagrant@kvm1 ~]$ sudo dnf install opennebula-node-kvm
````
🌞 **Démarrer le service `libvirtd`**

- activer le au démarrage de la machine 
````bash
[vagrant@kvm1 ~]$ sudo systemctl start libvirtd
[vagrant@kvm1 ~]$ sudo systemctl enable libvirtd
````

## B. Système

🌞 **Ouverture firewall**

````bash
[vagrant@kvm1 ~]$ sudo firewall-cmd --permanent --zone=public --add-port=22/tcp
success
[vagrant@kvm1 ~]$ sudo firewall-cmd --permanent --zone=public --add-port=8472/udp
success
````

| Port | Proto | Why ? |
|------|-------|-------|
| 22   | TCP   | SSH   |
| 8472 | UDP   | VXLAN |

🌞 **Handle SSH**

````bash
#après avoir ajouter la id.rsa.pub de frontend and kvm1 et kvm2
[oneadmin@frontend ~]$ ssh oneadmin@kvm1.one
Last login: Tue Apr  9 17:34:38 2024 from 10.3.1.11
[oneadmin@kvm1 ~]$ 
````

## C. Ajout des noeuds au cluster
Ajoutez le nouvel hôte KVM et assurez-vous qu'ils remontent en statut `ON`.
![capture opennebula](image.png)


# III. 3. Setup réseau


## C. Préparer le bridge réseau

➜ **Ces étapes sont à effectuer uniquement sur `kvm1.one`** dans un premier temps

- dans la partie IV du TP, quand vous mettrez en place `kvm2.one`, il faudra aussi refaire ça dessus

🌞 **Créer et configurer le bridge Linux**, j'vous file tout, suivez le guide :

```bash
[vagrant@kvm1 ~]$ sudo !!
sudo ip link add name vxlan_bridge type bridge
[vagrant@kvm1 ~]$ sudo !!
sudo ip link set dev vxlan_bridge up 
[vagrant@kvm1 ~]$ sudo ip addr add 10.220.220.201/24 dev vxlan_bridge
[vagrant@kvm1 ~]$ sudo firewall-cmd --add-interface=vxlan_bridge --zone=public --permanent
Error: [Errno 13] Permission denied: '/etc/sysconfig/network-scripts/ifcfg-eth1'
[vagrant@kvm1 ~]$ sudo firewall-cmd --add-interface=vxlan_bridge --zone=public --permanent
Warning: ALREADY_ENABLED: vxlan_bridge
success
[vagrant@kvm1 ~]$ sudo firewall-cmd --add-masquerade --permanent
success
[vagrant@kvm1 ~]$ sudo !!
sudo firewall-cmd --reload
success
[vagrant@kvm1 ~]$ 

```

# Setup KVM 2 Sachant que j'ai fait tout la conf jusqu'a la partie Réseau

````bash
[vagrant@kvm2 .ssh]$ sudo !!
sudo ip link add name vxlan_bridge type bridge
[vagrant@kvm2 .ssh]$ sudo !!
sudo ip link set dev vxlan_bridge up 
[vagrant@kvm2 .ssh]$ sudo ip addr add 10.220.220.202/24 dev vxlan_bridge
[vagrant@kvm2 .ssh]$ sudo firewall-cmd --add-interface=vxlan_bridge --zone=public --permanent
Error: [Errno 13] Permission denied: '/etc/sysconfig/network-scripts/ifcfg-eth1'
[vagrant@kvm2 .ssh]$ sudo firewall-cmd --add-interface=vxlan_bridge --zone=public --permanent
Warning: ALREADY_ENABLED: vxlan_bridge
success
[vagrant@kvm2 .ssh]$ sudo firewall-cmd --add-masquerade --permanent
success
[vagrant@kvm2 .ssh]$ sudo !!
sudo firewall-cmd --reload
success
````


# IV. Ajouter d'un noeud et VXLAN

## 1. Ajout d'un noeud

🌞 **Setup de `kvm2.one`, à l'identique de `kvm1.one`** excepté :

- une autre IP statique bien sûr
- idem, pour le bridge, donnez-lui l'IP `10.220.220.202/24` (celle qui est juste après l'IP du bridge de `kvm1`)
- une fois setup, ajoutez le dans la WebUI, dans `Infrastructure > Hosts`

![kvm2 ON](image2.png)

## 2. VM sur le deuxième noeud

🌞 **Lancer une deuxième VM**

![création vm kvm2.one](image3.png)

````bash
[oneadmin@frontend ~]$ eval $(ssh-agent)
Agent pid 48633
[oneadmin@frontend ~]$ ssh-add
Identity added: /var/lib/one/.ssh/id_rsa (oneadmin@frontend.one)
[oneadmin@frontend ~]$ ssh -A 10.3.1.22
Last login: Wed Apr 10 14:04:47 2024 from 10.3.1.11
[oneadmin@kvm2 ~]$ ssh root@10.220.220.2
Warning: Permanently added '10.220.220.2' (ED25519) to the list of known hosts.
[root@localhost ~]# 
````

## 3. Connectivité entre les VMs

🌞 **Les deux VMs doivent pouvoir se ping**

````bash
# Depuis la VM de kv1.one
[root@localhost ~]# ping 10.220.220.2
PING 10.220.220.2 (10.220.220.2) 56(84) bytes of data.
64 bytes from 10.220.220.2: icmp_seq=1 ttl=64 time=1.02 ms
64 bytes from 10.220.220.2: icmp_seq=2 ttl=64 time=1.79 ms
````

## 4. Inspection du trafic

🌞 **Téléchargez `tcpdump` sur l'un des noeuds KVM**

```bash
[vagrant@kvm1 ~]$ sudo tcpdump -i eth1 -w capt.pcap

[vagrant@kvm1 ~]$ sudo tcpdump -i vxlan_bridge -w capt2.pcap
```