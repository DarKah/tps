# TP1 : Héberger un service


🌞 **Boom ça commence direct : je veux l'état initial du firewall**

```bash
[darkah@tp1 ~]$ sudo firewall-cmd --list-all
[sudo] password for darkah: 
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 

```

🌞 **Fichiers /etc/sudoers /etc/passwd /etc/group** dans le dépôt de compte-rendu svp !

![/etc/passwd](./passwd) 
![/etc/sudoers](./sudoers)
![/etc/group](./group)

# Suite du TP

# I. Partie 1 : Host & Hack

**D'abord, on lance le machin, on voit ce que ça fait.**


🌞 **Télécharger l'application depuis votre VM**

```bash
sudo wget https://gitlab.com/it4lik/b3-csec-2024/-/blob/main/efrei_server
```

🌞 **Lancer l'application `efrei_server`**

```bash
[darkah@tp1 ~]LISTEN_ADDRESS=10.3.1.3 ./efrei_server
Warning: You should consider setting the environment variable LOG_DIR. Defaults to /tmp.
Server started. Listening on ('10.3.1.3', 8888)...

```

🌞 **Prouvez que l'application écoute sur l'IP que vous avez spécifiée**

```bash
[darkah@tp1 ~]$ ss -ltnp | grep 10.3.1.3
LISTEN 0      100         10.3.1.3:8888      0.0.0.0:*    users:(("main.bin",pid=1755,fd=6))

```

## 2. Prêts

🌞 **Se connecter à l'application depuis votre PC**

```bash
#j'ouvre le port 8888
[darkah@tp1 ~]$ sudo firewall-cmd --zone=public --add-port=8888/tcp --permanent
success
[darkah@tp1 ~]$ sudo firewall-cmd --reload
success

```

```bash
darkah@littlefire:~$ nc 10.3.1.3 8888
Hello ! Tu veux des infos sur quoi ?
1) cpu
2) ram
3) disk
4) ls un dossier

Ton choix (1, 2, 3 ou 4) :
```

## 3. Hackez

🌞 **Euh bah... hackez l'application !**

L'option 4 est la vuln, on peut totalement entrée du texte.
La commande renvoit dans tout les cas un ```ls``` donc déjà on peut clairement entrée une autre commande a la suite.
```bash
#j'ai try d'entrer ceci
/tmpp || sudo su
#l'application tente de ce connecter en sudo, donc en bonus elle est inutilisable après la requète

```

# II. Servicer le programme

## 1. Création du service

🌞 **Créer un service `efrei_server.service`**

```bash
#conf de mon fichier /etc/systemd/system/efrei_server.service
[Unit]
Description=Super serveur EFREI

[Service]
ExecStart=/usr/local/bin/efrei_server
Environment=LISTEN_ADDRESS=10.3.1.3
```

## 2. Tests

🌞 **Exécuter la commande `systemctl status efrei_server`**

```bash
[darkah@tp1 ~]$ systemctl status efrei_server
○ efrei_server.service - Super serveur EFREI
     Loaded: loaded (/etc/systemd/system/efrei_server.service; static)
     Active: inactive (dead)
```
🌞 **Démarrer le service**

```bash
[darkah@tp1 ~]$ sudo systemctl restart efrei_server
```

🌞 **Vérifier que le programme tourne correctement**

```bash
[darkah@tp1 ~]$ sudo systemctl status efrei_server
● efrei_server.service - Super serveur EFREI
     Loaded: loaded (/etc/systemd/system/efrei_server.service; static)
     Active: active (running) since Wed 2024-09-11 12:13:38 CEST; 5s ago
   Main PID: 1237 (efrei_server)
      Tasks: 2 (limit: 11113)
     Memory: 39.5M
        CPU: 71ms
     CGroup: /system.slice/efrei_server.service
             ├─1237 /tmp/efrei_server
             └─1238 /tmp/efrei_server

Sep 11 12:13:38 tp1 systemd[1]: Started Super serveur EFREI.

#comme le port de l'application est 888
[darkah@tp1 ~]$ ss -ltnp |grep 8888
LISTEN 0      100     10.3.1.3:8888      0.0.0.0:* #elle tourne donc bien sur la bonne ip


darkah@littlefire:~$ nc 10.3.1.3 8888
Hello ! Tu veux des infos sur quoi ?
1) cpu
2) ram
3) disk
4) ls un dossier

Ton choix (1, 2, 3 ou 4) :


```
