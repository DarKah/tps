# TP2 : Routage, DHCP et DNS

# Sommaire

- [TP2 : Routage, DHCP et DNS](#tp2--routage-dhcp-et-dns)
- [Sommaire](#sommaire)
- [II. Serveur DHCP](#ii-serveur-dhcp)
- [III. ARP](#iii-arp)
  - [1. Les tables ARP](#1-les-tables-arp)
  - [2. ARP poisoning](#2-arp-poisoning)


☀️ **Configuration de `router.tp2.efrei`**
- Je récupère d'abord le nom de mon interface réseau
```bash
[darkah@routeur ~]$ ip a

[darkah@routeur ~]$ 
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e8:f4:d6 brd ff:ff:ff:ff:ff:ff
```

ici on va utiliser **enp0s9**

```bash
sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s9
```

```bash
NAME=enp0s9
DEVICE=enp0s9

BOOTPROTO=dhcp
ONBOOT=yes
```

```bash
sudo nmcli con reload
```

```bash
sudo nmcli con up enp0s9

Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/5)
```

- pour l'IP statique
  
```bash
sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3
```
```bash
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.254
NETMASK=255.255.255.0
```
```bash
sudo nmcli con reload
sudo nmcli con up enp0s3
```

```bash
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e3:1e:db brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.254/24 brd 10.2.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fee3:1edb/64 scope link
       valid_lft forever preferred_lft forever

4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e8:f4:d6 brd ff:ff:ff:ff:ff:ff
    inet 192.168.122.165/24 brd 192.168.122.255 scope global dynamic noprefixroute enp0s9
       valid_lft 2892sec preferred_lft 2892sec
    inet6 fe80::a00:27ff:fee8:f4d6/64 scope link
       valid_lft forever preferred_lft forever
```
  


☀️ **Configuration de `node1.tp2.efrei`**

```bash
sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s3
```

```bash
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.1
NETMASK=255.255.255.0

```

```bash
sudo nmcli con reload
sudo nmcli con up enp0s3
```

```bash
[darkah@node1 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=1.87 ms
```
```bash
# ajouté dans /etc/sysconfig/network-scripts/ifcfg-enp0s3
GATEWAY=192.168.1.254
DNS1=1.1.1.1

```

```bash
sudo nmcli con reload
sudo nmcli con up enp0s3
```

```bash
[darkah@node1 ~]$ ping google.com
PING google.com (142.250.201.174) 56(84) bytes of data.
64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=1 ttl=111 time=28.8 ms
```

```bash
[darkah@node1 ~]$ traceroute google.com
traceroute to google.com (142.250.75.238), 30 hops max, 60 byte packets
 1  _gateway (10.2.1.254)  0.801 ms  0.817 ms  0.810 ms
 2  192.168.122.1 (192.168.122.1)  1.492 ms  1.439 ms  1.333 ms
 3  10.0.3.2 (10.0.3.2)  1.994 ms  1.927 ms  1.905 ms
```

# II. Serveur DHCP

➜ **Tableau d'adressage**

| Nom                | IP              |
| ------------------ | --------------- |
| `router.tp2.efrei` | `10.2.1.254/24` |
| `node1.tp2.efrei`  | `N/A`           |
| `dhcp.tp2.efrei`   | `10.2.1.253/24` |

☀️ **Install et conf du serveur DHCP** sur `dhcp.tp2.efrei`

```bash
# ajouté dans /etc/sysconfig/network-scripts/ifcfg-enp0s3
GATEWAY=192.168.1.254
DNS1=1.1.1.1

# ajouté dans la conf de enp0s3 de node1
GATEWAY=10.2.1.254
```


☀️ **Test du DHCP** sur `node1.tp2.efrei`

```bash
# nouvelle conf de enp0s3
NAME=enp0s3
DEVICE=enp0s3

GATEWAY=10.2.1.254

BOOTPROTO=dhcp
ONBOOT=yes

```

```bash
# ip de node
[darkah@node1 ~]$ ip a

2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:9c:7d:f0 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.1/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 478sec preferred_lft 478sec
    inet6 fe80::a00:27ff:fe9c:7df0/64 scope link
       valid_lft forever preferred_lft forever

# route
[darkah@node1 ~]$ ip route show
default via 10.2.1.254 dev enp0s3 proto dhcp src 10.2.1.1 metric 102

# ping 
[darkah@node1 ~]$ ping google.com
PING google.com (142.250.178.142) 56(84) bytes of data.
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=1 ttl=54 time=29.5 ms
```

🌟 **BONUS**

```bash
# ajouté à /etc/dhcp/dhcpd.conf
option domain-name-servers     1.1.1.1;
```

☀️ **Wireshark it !**

inclue dans le GIT

# III. ARP

## 1. Les tables ARP

☀️ **Affichez la table ARP de `router.tp2.efrei`**

```bash
# afficher la table de routage de routeur.tp2
[darkah@routeur ~]$ ip neigh show
10.2.1.253 dev enp0s3 lladdr 08:00:27:2e:ba:d3 DELAY
10.2.1.1 dev enp0s3 lladdr 08:00:27:9c:7d:f0 REACHABLE
```

☀️ **Capturez l'échange ARP avec Wireshark**


inclue dans le GIT

## 2. ARP poisoning

☀️ **Exécuter un simple ARP poisoning**

non réussi
