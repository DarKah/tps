# Mise en place de la topologie et routage

- [Mise en place de la topologie et routage](#mise-en-place-de-la-topologie-et-routage)
    - [Tableau d'adressage](#tableau-dadressage)
  - [I. Setup GNS3](#i-setup-gns3)
  - [II. Routes routes routes](#ii-routes-routes-routes)
- [ping](#ping)
- [Services Réseau](#services-réseau)
  - [Présentation](#présentation)
    - [Tableau d'adressage](#tableau-dadressage-1)
- [I. DHCP](#i-dhcp)
- [II. Serveur Web](#ii-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Page HTML et racine web](#2-page-html-et-racine-web)
  - [3. Config de NGINX](#3-config-de-nginx)
  - [4. Firewall](#4-firewall)
  - [5. Test](#5-test)
- [III. Serveur DNS](#iii-serveur-dns)
  - [3. Firewall](#3-firewall)
  - [4. Test](#4-test)
  - [5. DHCP my old friend](#5-dhcp-my-old-friend)

### Tableau d'adressage

| Machine          | Réseau 1        | Réseau 2        | Réseau 3        |
| ---------------- | --------------- | --------------- | --------------- |
| `node1.net1.tp3` | `10.3.1.11/24`  | nop             | nop             |
| `node2.net1.tp3` | `10.3.1.12/24`  | nop             | nop             |
| `router1.tp3`    | `10.3.1.254/24` | nop             | `10.3.100.1/30` |
| `router2.tp3`    | nop             | `10.3.2.254/24` | `10.3.100.2/30` |
| `node1.net2.tp3` | nop             | `10.3.2.11/24`  | nop             |
| `node2.net2.tp3` | nop             | `10.3.2.12/24`  | nop             |

## I. Setup GNS3

🌞 **Mettre en place la topologie dans GS3**

- reproduisez la topologie, configurez les IPs et les noms sur toutes les machines
- une fois en place, assurez-vous donc que :
  - toutes les machines du réseau 1 peuvent se `ping` entre elles :
   ```bash
   [darkah@node1net1 ~]$ ping 10.3.1.12
    PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
    64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=1.54ms
   ```
  - toutes les machines du réseau 2 peuvent se `ping` entre elles :
  ```bash
  [darkah@node1net2 ~]$ ping 10.3.2.12
  PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
  64 bytes from 10.3.2.12: icmp_seq=1 ttl=64 time=1.29 ms
  ```
  - toutes les machines du réseau 3 peuvent se `ping` entre elles :
  ```bash
  [darkah@routeur1 ~]$ ping 10.3.100.2
  PING 10.3.100.2 (10.3.100.2) 56(84) bytes of data.
  64 bytes from 10.3.100.2: icmp_seq=1 ttl=64 time=1.13 ms
  ```
- le `router1.tp3` doit avoir un accès internet normal
  - prouvez avec une commande `ping` qu'il peut joindre une IP publique connue :
  [darkah@routeur1 ~]$ ping 1.1.1.1
  PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
  64 bytes from 1.1.1.1: icmp_seq=1 ttl=52 time=20.5 ms
  - prouvez avec une commande `ping` qu'il peut joindre des machines avec leur nom DNS public (genre `efrei.fr`) :
  ```bash
  [darkah@routeur1 ~]$ ping efrei.fr
  PING efrei.fr (51.255.68.208) 56(84) bytes of data.
  64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208):     icmp_seq=1 ttl=49 time=28.3 ms
  ```

## II. Routes routes routes

🌞 **Mettre en place les routes locales**

- ajoutez les routes nécessaires pour que les membres du réseau 1 puissent joindre les membres du réseau 2 (et inversement)
```bash
# Node1.net1.tp3 et Node2.net1.tp3

#conf
10.3.2.0/24 via 10.3.1.254 dev enp0s3

#route show
[darkah@node1net1 ~]$ ip r s
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.11 metric 101
10.3.2.0/24 via 10.3.1.254 dev enp0s3
192.168.56.0/24 dev enp0s10 proto kernel scope link src 192.168.56.110 metric 100

[darkah@node2net1 ~]$ ip r s
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.12 metric 101
10.3.2.0 via 10.3.1.254 dev enp0s3
192.168.56.0/24 dev enp0s10 proto kernel scope link src 192.168.56.111 metric 100

# Node1.net2.tp3 et Node2.net2.tp3

#conf
10.3.1.0/24 via 10.3.2.254 dev enp0s3

#route show
[darkah@node1net2 ~]$ ip r s
10.3.1.0/24 via 10.3.2.254 dev enp0s3
10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.11 metric 101
192.168.56.0/24 dev enp0s10 proto kernel scope link src 192.168.56.113 metric 100

[darkah@node2net2 network-scripts]$ ip r s
10.3.1.0/24 via 10.3.2.254 dev enp0s3 proto static metric 101
10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.12 metric 101
192.168.56.0/24 dev enp0s10 proto kernel scope link src 192.168.56.112 metric 100

#routeur1 et routeur2

#conf routeur1
10.3.2.0/24 via 10.3.100.2 dev enp0s3

#conf routeur2
10.3.1.0/24 via 10.3.100.1 dev enp0s8

#route show
[darkah@routeur1 ~]$ ip r s
default via 192.168.122.1 dev enp0s8 proto dhcp src 192.168.122.196 metric 111
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.254 metric 101
10.3.2.0/24 via 10.3.100.2 dev enp0s9 proto static metric 112
10.3.100.0/30 dev enp0s9 proto kernel scope link src 10.3.100.1 metric 112
192.168.56.0/24 dev enp0s10 proto kernel scope link src 192.168.56.114 metric 100
192.168.122.0/24 dev enp0s8 proto kernel scope link src 192.168.122.196 metric 111

[darkah@routeur2 ~]$ ip r s
10.3.1.0/24 via 10.3.100.1 dev enp0s8 proto static metric 102
10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.254 metric 101
10.3.100.0/30 dev enp0s8 proto kernel scope link src 10.3.100.2 metric 102
192.168.56.0/24 dev enp0s10 proto kernel scope link src 192.168.56.115 metric 100
```

🌞 **Mettre en place les routes par défaut**

- faire en sorte que toutes les machines de votre topologie aient un accès internet, il faut donc :
  - sur les machines du réseau 1, ajouter `router.net1.tp3` comme passerelle par défaut :
  ```bash
  # conf
  GATEWAY=10.3.1.254
  
  # ping
  [darkah@node1net1 ~]$ ping google.com
  PING google.com (142.250.201.174) 56(84) bytes of data.
  64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=1 ttl=112 time=24.7 ms
  ```
  - sur les machines du réseau 2, ajouter `router.net2.tp3` comme passerelle par défaut :
  ```bash
   # conf
  default via 10.3.2.254 dev enp0s3

  # ping
  [darkah@node2net2 network-scripts]$ ping google.com
  PING google.com (142.250.179.78) 56(84) bytes of data.
  64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=1 ttl=110 time=17.4 ms
  ```
  - sur l`router.net2.tp3`, ajouter `router1.net.tp3` comme passerelle par défaut :
  ```bash
  # conf
  10.3.1.0/24 via 10.3.100.1 dev enp0s8

  # ping
  [darkah@routeur2 ~]$ ping google.com
  PING google.com (142.250.178.142) 56(84) bytes of data.
  64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=1 ttl=112 time=16.3 ms
  ```
- prouvez avec un `ping` depuis `node1.net1.tp3` que vous avez bien un accès internet :
    ```bash
  # ping
  [darkah@node1net1 ~]$ ping google.com
  PING google.com (142.250.201.174) 56(84) bytes of data.
  64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=1 ttl=112 time=24.7 ms
    ```
- prouvez avec un `traceroute` depuis `node2.net2.tp3` que vous avez bien un accès internet, et que vos paquets transitent bien par `router2.tp3` puis par `router1.tp3` avant de sortir vers internet :
  ```bash
  [darkah@node2net2 network-scripts]$ traceroute 1.1.1.1
  traceroute to 1.1.1.1 (1.1.1.1), 30 hops max, 60 byte packets
  1  _gateway (10.3.2.254)  0.614 ms  20.717 ms  20.724 ms
  2  10.3.100.1 (10.3.100.1)  0.959 ms  1.013 ms  0.992 ms
  3  192.168.122.1 (192.168.122.1)  1.701 ms  1.761 ms  1.687 ms
  4  10.0.3.2 (10.0.3.2)  1.714 ms  1.848 ms  1.860 ms
  ```
# Services Réseau

- [Mise en place de la topologie et routage](#mise-en-place-de-la-topologie-et-routage)
    - [Tableau d'adressage](#tableau-dadressage)
  - [I. Setup GNS3](#i-setup-gns3)
  - [II. Routes routes routes](#ii-routes-routes-routes)
- [ping](#ping)
- [Services Réseau](#services-réseau)
  - [Présentation](#présentation)
    - [Tableau d'adressage](#tableau-dadressage-1)
- [I. DHCP](#i-dhcp)
- [II. Serveur Web](#ii-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Page HTML et racine web](#2-page-html-et-racine-web)
  - [3. Config de NGINX](#3-config-de-nginx)
  - [4. Firewall](#4-firewall)
  - [5. Test](#5-test)
- [III. Serveur DNS](#iii-serveur-dns)
  - [3. Firewall](#3-firewall)
  - [4. Test](#4-test)
  - [5. DHCP my old friend](#5-dhcp-my-old-friend)

## Présentation

On va aller un peu plus loin dans cette partie et différencier nos deux réseaux :

- le réseau 1 représentera un réseau pour des clients classiques du réseau : les employés d'une entreprise par exemple
- le réseau 2 représentera la salle serveur de l'établissement : on y trouve donc les serveurs d'infrastructure

Ainsi, dans cette partie, on va monter plusieurs services, orientés réseau, que l'on retrouve dans la plupart des réseaux classiques.

**Au menu :**

➜ **Serveur DHCP, again !**

- sur une machine `dhcp.net1.tp3`
- comme d'hab, il filera des IP à nos clients, et leur indiquera aussi comment joindre internet
- il a besoin d'être dans le même réseau que les clients à qui il file des IPs, il sera donc dans le réseau 1
- à l'inverse, on attribue aux serveurs des IPs statiques, pas de DHCP pour les serveurs !

➜ **Serveur Web**

- sur une machine `web.net2.tp3`
- il hébergera un bête site web d'accueil, qui symbolisera un site web d'entreprise par exemple
- il sera dans le réseau 2

➜ **Serveur DNS**

- sur une machine `dns.net2.tp3`
- il permettra à toutes les machines de notre parc de se joindre avec leurs noms respectifs
- ainsi on pourra accéder au site web en utilisant l'adresse `http://web.net2.tp3` dans le navigateur d'un des clients


### Tableau d'adressage

| Machine          | Réseau 1        | Réseau 2        | Réseau 3        |
| ---------------- | --------------- | --------------- | --------------- |
| `node1.net1.tp3` | `10.3.1.11/24`  | nop             | nop             |
| `dhcp.net1.tp3`  | `10.3.1.253/24` | nop             | nop             |
| `router1.tp3`    | `10.3.1.254/24` | nop             | `10.3.100.1/30` |
| `router2.tp3`    | nop             | `10.3.2.254/24` | `10.3.100.2/30` |
| `web.net2.tp3`   | nop             | `10.3.2.101/24` | nop             |
| `dns.net2.tp3`   | nop             | `10.3.2.102/24` | nop             |

Remarquez dans le tableau d'adressage que :

- il n'y a plus de clients dans le réseau 2
  - ouais bah c'est la salle serveur, pas de PC client dans la salle serveur, que des serveurs !
- il reste un client dans le réseau 1
  - normal : c'est le réseau des clients, c'est avec eux qu'on testera que tout fonctionne

> Ne vous gênez pas pour recycler `node1.net2.tp3` et `node2.net2.tp3` en `web.net2.tp3` et `dns.net2.tp3`. Veillez à bien reconfigurer les IPs et les noms d'hôte. Aussi, n'hésitez pas à descendre la RAM allouée aux VMs si ça commence à faire beaucoup de machines.

Une fois la topologie légèrement modifiée, les machines ré-adressées et renommées, passez à la suite, dans l'ordre :

# I. DHCP

Bon, vous devez être rodés nan maintenant ?!

🌞 **Setup de la machine `dhcp.net1.tp3`**

```bash
## create new
# specify domain name
option domain-name     "Cloudflare";
# specify DNS server's hostname or IP address
option domain-name-servers     1.1.1.1;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.3.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.1.50 10.3.1.99;
    # specify broadcast address
    option broadcast-address 10.3.1.255;
    # specify gateway
    option routers 10.3.1.254;
}

```

🌞 **Preuve !**

- effectuer une demande d'adresse IP depuis `node1.net1.tp3`
```bash
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=dhcp
ONBOOT=yes

DNS1=1.1.1.1
```
- montrez l'IP obtenue
```bash
  2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:24:d8:3a brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.50/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s3
  ```
- montrez que votre table de routage a été mise à jour
```bash
[darkah@node1net1 ~]$ ip route show
default via 10.3.1.254 dev enp0s3 proto dhcp src 10.3.1.50 metric 101
```
- montrez l'adresse du serveur DNS que vous utilisez
```bash
[darkah@node1net1 ~]$ cat /etc/resolv.conf
# Generated by NetworkManager
search efrei.fr
nameserver 1.1.1.1
```
- prouvez que vous avez un accès internet normal avec un `ping`
```bash
[darkah@node1net1 ~]$ ping google.com
PING google.com (216.58.211.206) 56(84) bytes of data.
64 bytes from mrs09s11-in-f14.1e100.net (216.58.211.206): icmp_seq=1 ttl=113 time=20.7 ms  
```


# II. Serveur Web

- [Mise en place de la topologie et routage](#mise-en-place-de-la-topologie-et-routage)
    - [Tableau d'adressage](#tableau-dadressage)
  - [I. Setup GNS3](#i-setup-gns3)
  - [II. Routes routes routes](#ii-routes-routes-routes)
- [ping](#ping)
- [Services Réseau](#services-réseau)
  - [Présentation](#présentation)
    - [Tableau d'adressage](#tableau-dadressage-1)
- [I. DHCP](#i-dhcp)
- [II. Serveur Web](#ii-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Page HTML et racine web](#2-page-html-et-racine-web)
  - [3. Config de NGINX](#3-config-de-nginx)
  - [4. Firewall](#4-firewall)
  - [5. Test](#5-test)
- [III. Serveur DNS](#iii-serveur-dns)
  - [3. Firewall](#3-firewall)
  - [4. Test](#4-test)
  - [5. DHCP my old friend](#5-dhcp-my-old-friend)


## 1. Installation

> **L'install et la config sont réalisé sur la machine `web.net2.tp3`.**

🌞 **Installation du serveur web NGINX**

- installez le paquet `nginx`
```bash
[darkah@webnet2 ~]$ sudo dnf install nginx
```

## 2. Page HTML et racine web

🌞 **Création d'une bête page HTML**

- on va créer un nouveau dossier qui hébergera tous les fichiers de notre site (bon là y'en aura qu'un, et il sera moche, c'est un détail)
- créez le dossier `/var/www/efrei_site_nul/`
```bash
[darkah@webnet2 www]$ mkdir efrei_site_nul/
```

- faites le appartenir à l'utilisateur `nginx` (sinon le contenu du dossier ne sera pas accessible par le serveur Web NGINX, et il ne pourra pas servir le site !)
  - ça se fait avec une commande `chown`
```bash
[darkah@webnet2 www]$ sudo chown nginx /var/www/efrei_site_nul/
[darkah@webnet2 www]$ ls -l
total 0
drwxr-xr-x. 2 nginx root 6 Oct 24 10:18 efrei_site_nul
```
- créez un fichier texte `/var/www/efrei_site_nul/index.html` avec la phrase de votre choix à l'intérieur
```bash
[darkah@webnet2 efrei_site_nul]$ sudo !!
sudo touch index.html
[darkah@webnet2 efrei_site_nul]$ ls
index.html

#Dans le fichier
Ce site est nul
```
  - ce fichier aussi doit appartenir à l'utilisateur `nginx`
```bash
[darkah@webnet2 efrei_site_nul]$ sudo chown nginx index.html
[darkah@webnet2 efrei_site_nul]$ ls -l
total 4
-rw-r--r--. 1 nginx root 16 Oct 24 10:32 index.html
```

## 3. Config de NGINX

🌞 **Création d'un fichier de configuration NGINX**

- créez le fichier `/etc/nginx/conf.d/web.net2.tp3.conf` et ajoutez le contenu suivant :
```bash
sudo touch /etc/nginx/conf.d/web.net2.tp3.conf
```

```nginx
# dans le fichier /etc/nginx/conf.d/web.net2.tp3.conf
  server {
      server_name   web.net2.tp3;

      listen        10.3.2.101:80;

      location      / {
          root      /var/www/efrei_site_nul;

          index index.html;
      }
  }
```

## 4. Firewall

🌞 **Ouvrir le port nécessaire dans le firewall**
- comme indiqué dans la conf, on va servir le site sur le port standard : 80
```bash
[darkah@webnet2 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
[sudo] password for darkah:
success
```

- vérifiez avec une deuxième commande que le port est bien actuellement ouvert dans le firewall
```bash
[darkah@webnet2 ~]$ sudo firewall-cmd --reload
success
[darkah@webnet2 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s10 enp0s3
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## 5. Test

🌞 **Démarrez le service NGINX !**

- s'il y a des soucis, lisez bien les lignes d'erreur, et n'hésitez pas à m'appeler

```bash
# Démarrez le service tout de suite
$ sudo systemctl start nginx

[darkah@webnet2 ~]$ sudo systemctl start nginx


# Faire en sorte que le service démarre tout seul quand la VM s'allume
$ sudo systemctl enable nginx

[darkah@webnet2 ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.

# Obtenir des infos sur le service
$ sudo systemctl status nginx

[darkah@webnet2 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; preset>
     Active: active (running) since Thu 2023-10-26 09:38:30 CEST; 27s ago
   Main PID: 1421 (nginx)
      Tasks: 2 (limit: 4611)
     Memory: 3.3M
        CPU: 160ms
     CGroup: /system.slice/nginx.service
             ├─1421 "nginx: master process /usr/sbin/nginx"
             └─1422 "nginx: worker process"
```

🌞 **Test local**

- vous pouvez visiter le site web en local, depuis la ligne de commande de la machine `web.net2.tp3`, avec la commande `curl` : par exemple `curl http://10.3.2.101`

```bash
[darkah@node1net1 ~]$ curl http://10.3.2.101
Ce site est nul
```

🌞 **Accéder au site web depuis un client**

```bash
# depuis node1.net1.tp3

[darkah@dhcpnet1 ~]$ curl http://10.3.2.101
Ce site est nul
```

🌞 **Avec un nom ?**

- utilisez le fichier `hosts` de votre machine client pour accéder au site web en saissant `http://web.net2.tp3` (ce qu'on avait écrit dans la conf quoi !)
```bash
# dans /etc/hosts
10.3.2.101 web web.net2.tp3

[darkah@node1net1 ~]$ curl http://web.net2.tp3
Ce site est nul
```

# III. Serveur DNS

- [III. Serveur DNS](#iii-serveur-dns)
  - [2. Config](#2-config)
  - [3. Firewall](#3-firewall)
  - [4. Test](#4-test)
  - [5. DHCP my old friend](#5-dhcp-my-old-friend)

## 3. Firewall

🌞 **Ouvrir le port nécessaire dans le firewall**

- ouvrez donc à l'aide d'une commande ce port UDP dans le firewall de `dns.
```bash
[darkah@dnsnet2 ~]$ sudo firewall-cmd --add-port=53/udp --permanent
success
```

## 4. Test

Depuis une machine cliente du réseau, utilisez la commande `dig` pour faire des requêtes DNS à la main.

La commande `dig` sert à effectuer des requêtes DNS à la main depuis une machine Linux (on l'a obtenu en téléchargeant le paquet `bind-utils` quand on a install Rocky ensemble). Elle s'utilise comme suit :

```bash
# faire une requête DNS en utilisant le serveur DNS connu par l'OS
$ dig efrei.fr

# faire une requête DNS en précisant à quel serveur DNS on pose la question
$ dig efrei.fr @1.1.1.1

# faire une requête DNS inverse (trouver le nom qui correspond à une IP)
$ dig -x 10.3.2.101
```

🌞 **Depuis l'une des machines clientes du réseau 1** (par exemple `node1.net1.tp3`)

- utiliser `dig` pour trouver à quelle IP correspond le nom `web.net2.tp3`

```bash
[darkah@node1net1 ~]$ dig web.net2.tp3

; <<>> DiG 9.16.23-RH <<>> web.net2.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 10607
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;web.net2.tp3.                  IN      A

;; AUTHORITY SECTION:
.                       86400   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2023102600 1800 900 604800 86400

;; Query time: 25 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Oct 26 11:04:04 CEST 2023
;; MSG SIZE  rcvd: 116
```

- utiliser `curl` pour visiter le site web sur `web.net2.tp3` en utilisant son nom

```bash
[darkah@node1net1 ~]$ curl web.net2.tp3
Ce site est nul
```

## 5. DHCP my old friend

🌞 **Editez la configuration du serveur DHCP sur `dhcp.net1.tp3`**

```bash
# dans la conf du dhcp j'ai changé 1.1.1.1 par :

option domain-name-servers     10.3.2.102;



[darkah@node1net1 ~]$ dig 10.3.2.102

; <<>> DiG 9.16.23-RH <<>> 10.3.2.102
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 21132
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: cfb99cf979501d9101000000653a3d35e0a4a4afd91f49ea (good)
;; QUESTION SECTION:
;10.3.2.102.                    IN      A

;; AUTHORITY SECTION:
.                       10776   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2023102600 1800 900 604800 86400

;; Query time: 8 msec
;; SERVER: 10.3.2.102#53(10.3.2.102)
;; WHEN: Thu Oct 26 12:19:33 CEST 2023
;; MSG SIZE  rcvd: 142
```
